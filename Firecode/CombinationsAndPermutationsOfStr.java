// getCombPerms("a") ==> {"a"}
// getCombPerms("ab") ==> {"a","ab","ba","b"}

public static ArrayList<String> getCombPerms(String s) {

    if(s==null) return null;
    ArrayList<String> combAndPerms = new ArrayList<String>();

    calculateCombAndPerms(s, "", combAndPerms);
    return combAndPerms;
}

public static void calculateCombAndPerms(String s, String c, ArrayList<String> list) {
    if(s.length() == 0) return;

    for(int i=0; i<s.length(); i++) {
        char ch = s.charAt(i);
        String sWithoutCh = i==0 ? s.substring(i+1) : s.substring(0,i) + s.substring(i+1);
        
        list.add(c+ch);
        calculateCombAndPerms(sWithoutCh, c+ch, list);
    }
}


