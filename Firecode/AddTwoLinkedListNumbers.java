// Input 1: 1->2->3
// Input 2: 1->2->3  
// Output : 2->4->6 
// 
// Input 1: 9->9->9
// Input 2: 9->8 
// Output : 8->8->0->1
// 
// Note: You can assume that the input integers are non negative and the digits stored in the linked lists are in the reverse order

// java.util.* and java.util.streams.* have been imported for this problem.
// You don't need any other imports.

public static ListNode sumTwoLinkedLists(ListNode input1, ListNode input2) {
    if(input1 == null && input2 == null) return null;
    if(input1 == null && input2 != null) return input2;
    if(input1 != null && input2 == null) return input1;

    int sumDigit=0, carry=0;

    ListNode c1=input1, c2=input2, sum=null, sumHead=null, temp=null;
    while(c1 !=null && c2 != null) {
       sumDigit = (c1.data + c2.data + carry) % 10;
       carry = (c1.data + c2.data + carry) / 10;
       temp = new ListNode(sumDigit);
       
       if(sum == null) { sumHead = temp; }
       else { sum.next = temp; }

       sum = temp;
       c1=c1.next;
       c2=c2.next;
    }

    if(c1==null) {
        while(c2!=null) {
            sumDigit = (c2.data + carry) % 10;
            carry = (c2.data + carry) / 10;
            sum.next = new ListNode(sumDigit);
            sum = sum.next;
            c2 = c2.next;
        }
    }

    if(c2==null) { 
        while(c1!=null) { 
            sumDigit = (c1.data + carry) % 10;
            carry = (c1.data + carry) / 10;
            sum.next = new ListNode(sumDigit);
            sum = sum.next;
            c1 = c1.next;
        }
    }

    if(carry != 0) {
        sum.next = new ListNode(carry);
    }

    return sumHead;
}
