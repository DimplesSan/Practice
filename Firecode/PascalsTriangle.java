public static ArrayList<ArrayList<Integer>> generatePascalTriangle(int numRows) {
    
    ArrayList<ArrayList<Integer>> pascalsTriangle = new ArrayList<ArrayList<Integer>>();
    for(int i=0; i<numRows; i++) {
        ArrayList<Integer> row = new ArrayList<Integer>();

        for(int j=0; j<=i; j++) {
            int pascalNum = getPascalNum(i, j, pascalsTriangle);
            row.add(pascalNum);
        }

        pascalsTriangle.add(row);
    } 

    return pascalsTriangle;
}

public static int getPascalNum(int i, int j, ArrayList<ArrayList<Integer>> pascalsTriangle) {
    if(i==0 || j==0) return 1;
    ArrayList<Integer> prevRow = pascalsTriangle.get(i-1);
    
    if(j>=prevRow.size()) {
        return prevRow.get(j-1);
    }

    return prevRow.get(j-1) + prevRow.get(j);
}
