// Example
// 
// Given 1->2->3->4,
// 
//       reverseInPairs(1) ==> 2->1->4->3
//
public ListNode reverseInPairs(ListNode head) {
        
    if(head == null || head.next == null) return head;

    // By manipulating data
//    ListNode n1=head, n2=head.next;
//    int temp;
//    while(n2!=null) {
//        temp = n2.data;
//        n2.data = n1.data;
//        n1.data = temp;
//
//        n1=n2.next;
//        n2= n1==null ? null : n1.next;
//    }

    // By manipulatin refs
    ListNode n1=head, n2=head.next, prev=null, temp=null, newHead=null;
    while(n2!=null) {
        if(n1==head) newHead = n2;

        temp = n2.next;
        n2.next = n1;
        n1.next = temp;
        if(prev!=null) prev.next = n2;

        prev=n1;
        n1 = temp 
        n2 = temp == null ? null : temp.next;
    }

    return newHead;
}

