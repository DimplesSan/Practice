// editDistance("sale", "sold") => 2
// Operations : 
// 1) Replace "a" with "o"
// 2) Replace "e" with "d")
// 
// 
// editDistance("sale", "sales") => 1
// Operations : 
// 1) Insert "s")
// 
// 
// editDistance("sa", "s") => 1
// Operations : 
// 1) Delete "a"

public int editDistance(String a, String b){
    int rows = a.length() + 1;
    int cols = b.length() + 1;

    int[][] distances = new int[rows][cols];
    for(int i=0; i<rows; i++) {
        for(int j=0; j<cols; j++) {
            if(i==0) distances[i][j] = j; // a is empty

            else if(j==0) distances[i][j] = i; // b is empty

            
            else if(a.charAt(i-1) == b.charAt(j-1)) distances[i][j] = distances[i-1][j-1];

            else {
                distances[i][j] = 1 + Math.min(
                        Math.min(distances[i][j-1], distances[i-1][j]),
                        distances[i-1][j-1]
                );
            }
        }
    }

    return distances[rows-1][cols-1];
   // return editDistance(a, b, a.length(), b.length());
}

public int editDistance(String a, String b, int aLen, int bLen) {
    if(aLen==0) return bLen;

    if(bLen==0) return aLen;

    if(a.charAt(aLen-1) == b.charAt(bLen-1)) return editDistance(a, b, aLen-1, bLen-1);

    int replaceDistance = editDistance(a, b, aLen-1, bLen-1);
    int insertDistance = editDistance(a, b, aLen, bLen-1);
    int deleteDistance = editDistance(a, b, aLen-1, bLen);
    
    return 1 + Math.min(Math.min(insertDistance, deleteDistance), replaceDistance);
}
