// Input Matrix : 
             
//     1 2 3
//     4 5 6
//     7 8 9

// Output  : 1 + 4 + 7 + 8 + 9 = 29

public static int matrixMaxSumDP(int[][] grid) {
    if(grid == null || grid.length < 1 || grid[0].length < 1) return 0;
    int rows = grid.length;
    int cols = grid[0].length;
    int[][] sums = new int[rows][cols];

    for(int i=0; i<rows; i++) {
        for(int j=0; j<cols; j++) {
            sums[i][j] = Math.max(getSumAt(i-1,j, sums), getSumAt(i,j-1, sums)) + grid[i][j]; 
        } 
    }

    return sums[rows-1][cols-1];
}
public static int getSumAt(int i, int j, int[][] sums) {
    if(i<0 || i>sums.length-1 || j<0 || j>sums[0].length-1) return 0;

    return sums[i][j];
}
