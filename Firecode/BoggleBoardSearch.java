
public static boolean boggleSearch(char[][] board, String word){
    boolean isFound = false;

    if(board == null || board.length == 0 || board[0].length == 0) return isFound;

    for(int i=0; i<board.length; i++) {
        for(int j=0; j<board[0].length; j++) {
            isFound = search(i, j, word, board, "");
            if(isFound) return true;
        }
    }

    return isFound;
}
        
public static boolean search(int i, int j, String word, char[][] board, String curStr) {
    if(i<0 || i > board.length-1 || j < 0 || j > board[0].length-1 || board[i][j] == '*' || !word.contains(curStr)) {
        return false;
    }
    
    if(word.equals(curStr)) return true;

    char temp = board[i][j];
    board[i][j] = '*';
    
    boolean isFound = search(i-1, j, word, board, curStr+temp) ||
        search(i+1, j, word, board, curStr+temp) ||
        search(i, j-1, word, board, curStr+temp) ||
        search(i, j+1, word, board, curStr+temp);

    board[i][j] = temp;

    return isFound;
}
