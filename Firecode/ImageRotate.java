    //Clockwise = Transpose and then Vertical Flip
    // Anti-Clockwise = Transpose and then Horizontal Flip
    
    return flipVertical(transpose(matrix));

}


public static int[][] transpose(int[][] m) {
    // For nXn diagonal stays the same
    int temp = 0;
    for(int i=0; i<m.length; i++) {
        for(int j=i; j<m[0].length; j++) {
            temp = m[i][j];
            m[i][j] = m[j][i];
            m[j][i] = temp;
        }
    }

    return m;
}

public static int[][] flipVertical(int[][] m) {
    if(m==null || m.length < 1 || m[0].length<1 ) return m;
    int temp=0;

    for(int i=0; i<m.length; i++) {
        for(int j=0, k=m[0].length-1; j<k;) {
            temp = m[i][j];
            m[i][j] = m[i][k];
            m[i][k] = temp;

            j++;
            k--;
        }
    }

    return m;
}
