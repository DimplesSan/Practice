// makeChange({25,10,5,1},10) => 4)
public static int makeChange(int[] coins, int amount) {
    return makeChange(coins, amount, 0);
}

public static int makeChange(int[] coins, int amount, int startCoin) {
    if(amount < 0) return 0;
    if(amount == 0) return 1;

    int numOfDiffCoinsNeeded = 0;
    for(int i=startCoin; i<coins.length; i++) {
        numOfDiffCoinsNeeded = numOfDiffCoinsNeeded + makeChange(coins, amount-coins[i], i);
    }

    return numOfDiffCoinsNeeded;
}
