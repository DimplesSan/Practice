// Example:
//   1
//  2 3
// 4 5 6 7
//8
//
//Output = 2
//Note: In case the tree is empty, return -1
//And:  Assume that root is at level 0.

public int findMaxSumLevel(TreeNode root) { 
    if(root == null) return -1;

    Queue<TreeNode> q = new LinkedList<TreeNode>();
    q.add(root);
    
    int currentLevelSum=0, currentLevel=0, maxLevel=0, maxSum=0;
    int currentChild=1;
    double maxChildren = Math.pow(2, currentLevel);
    TreeNode currentNode = null;

    while(!q.isEmpty()) {
        currentNode = q.poll();
        currentChild++;

        if(currentNode != null) {
            q.add(currentNode.left);
            q.add(currentNode.right);

            currentLevelSum = currentLevelSum + currentNode.data;
        } else {
            maxChildren = maxChildren - 2;
        }
        
        if(currentChild > maxChildren) {
            //All nodes in level processed
            if(currentLevelSum >= maxSum) {
                maxLevel = currentLevel;
                maxSum   = currentLevelSum;
                
            }

            currentChild = 1;
            currentLevel++;
            currentLevelSum = 0;
            maxChildren = Math.pow(2, currentLevel);
        }
    }

    return maxLevel;
)
