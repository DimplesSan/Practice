// Write a method to check whether an equation has 
// a balanced number of left and right parentheses and brackets - (including (,),[,],{}]))
// 
// isBalanced("() [] ()") ==> true
// isBalanced("([)]") ==> false

public static boolean isBalanced(String input) {

    if(input == null || input.length() ==0 ) return true;

    Stack<Character> s = new Stack<Character>();
    return isBalanced(input, s);

//    for(char c : input.toCharArray()) {
//        if(c == '(' || c == '[' || c == '{' || c == '<') 
//            s.push(c);
//        else {
//            if(s.empty()) return false; // To handle closing chars when chars in string are still available
//            
//            if(c == ')') { 
//                if(s.peek() == '(') { s.pop(); continue; }
//                else return false;
//            }
//
//            if(c == ']') {
//                if(s.peek() == '[') { s.pop(); continue; }
//                else return false;
//            }
//
//            if(c == '}') {
//                if(s.peek() == '{') { s.pop(); continue; }
//                else return false;
//            }
//        }
//    }
//    
//    return s.empty() ? true : false;
}

public static boolean isBalanced(String ip, Stack<Character> s) {
    if(ip.length() == 0) { return s.empty(); }

    char c =  ip.charAt(0);
    if(c == '(' || c == '[' || c == '{' || c == '<') {
        s.push(c);
        return isBalanced(ip.substring(1), s);
    }
    else if (c == ')' || c == ']' || c == '}' || c == '>') {
        //closing
        if(s.empty()) return false;

        if(c==')') {
            if(s.peek() == '(') {
                s.pop();
                return isBalanced(ip.substring(1), s);
            } else return false;
        }


        if(c==']') {
            if(s.peek() == '[') {
                s.pop();
                return isBalanced(ip.substring(1), s);
            } else return false;
        }


        if(c=='}') {
            if(s.peek() == '{') {
                s.pop();
                return isBalanced(ip.substring(1), s);
            } else return false;
        }
    }

    return isBalanced(ip.substring(1), s);
}
