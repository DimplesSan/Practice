public int getNodeDistance(TreeNode root, int n1, int n2) {
    ArrayList<TreeNode> n1Ancestors = new ArrayList<TreeNode>();
    ArrayList<TreeNode> n2Ancestors = new ArrayList<TreeNode>();

    getAncestors(root, n1, n1Ancestors);
    getAncestors(root, n2, n2Ancestors);

    Iterator<TreeNode> itn1 = n1Ancestors.iterator();
    Iterator<TreeNode> itn2 = n2Ancestors.iterator(); 

    //Ignore the first root node as it common
    itn1.next();
    itn2.next();
    int distance = 0;
    Iterator<TreeNode> it = n1Ancestors.size() >= n2Ancestors.size() ? itn1 : itn2;
    while(it.hasNext()) {
        TreeNode tn1 = itn1.hasNext() ? itn1.next() : null;
        TreeNode tn2 = itn2.hasNext() ? itn2.next() : null;
        if(tn1 != null && tn2 != null) {
            distance = tn1.data == tn2.data ? distance : distance+2;
        }
        else {
            distance++;
        }
    }

    return distance;
}

public boolean getAncestors(TreeNode node, int n, ArrayList<TreeNode> ancestors) {
    if(node==null) return false;

    ancestors.add(node);
    if(node.data==n) return true;

    boolean isNodeInLeftSubTree = false;
    isNodeInLeftSubTree = getAncestors(node.left, n, ancestors);
    if(isNodeInLeftSubTree) return true;

    boolean isNodeInRightSubTree = false;
    isNodeInRightSubTree = getAncestors(node.right, n, ancestors);
    if(isNodeInRightSubTree) return true;

    ancestors.remove(node); // Remove node from list ancestors as it wasn't found in the tree at all.
    return false;
}
