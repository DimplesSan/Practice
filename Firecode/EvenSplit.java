public static boolean splitArray(int[] arr) {
    // Find Sum of all elements
    // If odd then no possibility of 2 parts with equal sub-sums
    // Even then susbset sum problem with target as half of total
    
    int total=0
    for(int i: arr) { total =  total+i; }
    
    if(total%2 != 0 ) return false;
    
    return subsetSum(arr, total/2, 0);
}

public static boolean subsetSum(int[] arr, int target, int i) {
    boolean[][] subsets = new boolean[target+1][arr.length+1];
    for(int k=0; k<target; k++) 
        subsets[k][0] = false; // target isn't 0 but arr already exhausted

    for(int k=0; k<arr.length; k++)
        subsets[0][k] = true; // if target is 0 then remaining elements can be excluded to match it

    for(int j=1; j<=target; j++) {
        for(int k=1; k<=arr.length; k++) {
            if(j >= arr[k-1]) {
                subsets[j][k] = subsets[j][k-1] || subsets[j-arr[k-1]][k-1];
            }
        }
    }

    return subsets[target][arr.length];
//    if(i>=arr.length) return false;
//    if(target==0) return true;
//
//    if(arr[i] > target) subsetSum(arr, target, i+1);
//
//    return subsetSum(arr, target-arr[i], i+1) || subsetSum(arr, target, i+1);
}
