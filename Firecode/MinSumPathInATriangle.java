// Input Triangle: 
// 
// [   [1],
// 
//    [2, 3],
// 
//   [4, 5, 6],
// 
// [7, 8, 9, 10],
// 
// ]
// 
// 
// 
// Output : 14 (1->2->4->7)

// An adjacent node is defined as a node that is reached by moving down and left or down and right from a level. For eg, in the triangle shown below, if you are at the digit 3 in the second row, its adjacent nodes are 5 and 6

public static int minTriangleDepth(ArrayList<ArrayList<Integer>> input) {
    if(input == null || input.size() == 0) return 0;
        
    int height = input.size();
    ArrayList<Integer> base = input.get(height-1);
    int[] res = new int[base.size()]; // Buffer for widest list of numbers

    //Initialize with widest list of numbers
    for(int i=0; i<base.size(); i++) { res[i] = base.get(i); }

    for(int rowIdx=height-2; rowIdx>=0; rowIdx--) {
        ArrayList<Integer> row = input.get(rowIdx);

        for(int j=0; j<row.size(); j++) {
            res[j] = row.get(j) + Math.min(res[j], res[j+1]); // ith of row associated with only j and j+1 of res (or following row)
        }
    }

    return res[0];
    
}

public static int minTriangleDepth(int i, int j, ArrayList<ArrayList<Integer>> input) {
    if(i<0 || i>input.size()-1 || j<0 || j>input.get(i).size()-1) return 0;
      
    return input.get(i).get(j) + Math.min(
        minTriangleDepth(i+1, j, input),
        minTriangleDepth(i+1, j+1, input)
    );
}
